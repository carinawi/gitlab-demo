# CI/CD Deployment to Amazon S3 using GitLab

This repository contains the YAML-code used in [this](https://www.youtube.com/watch?v=mGFS2zUL8mo) video tutorial. The tutorial demonstrates how to create a CI/CD pipeline in GitLab for deploying a HTML-file to Amazon S3. Moreover, it covers the creation of a custom GitLab runner.

## Prerequisites: 
- GitLab account (https://about.gitlab.com)
- AWS account (https://aws.amazon.com/s3/) 

## Contents of repository

The YAML-code can be found in the [.gitlab-ci.yml file](https://gitlab.com/carinawi/gitlab-demo/-/blob/master/.gitlab-ci.yml) and has been adapted from the [documentation](https://about.gitlab.com/blog/2021/02/05/ci-deployment-and-environments/). For more information regarding the code, we refer to this article, in particular the section ``The first automated deployment``. 

The HTML-file is just a template HTML-file, and for the purpose of the demo, it can be replaced with any HTML-file with a name of your choice.

## Interested in learning more?
- For more information regarding the usage of S3, we refer to the [S3 documentation](https://docs.aws.amazon.com/AmazonS3/latest/userguide/Welcome.html).
- For further information regarding hosting static webpages using S3, we refer to [this page](https://docs.aws.amazon.com/AmazonS3/latest/userguide/WebsiteHosting.html). 
